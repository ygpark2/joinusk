
function top_menu(key) {
    $.getJSON('/joko/top/menu/' + key, function(data) {
        $('#middlebar').empty();
        $('#topbar > a').each(function () { 
            if ( $(this).css("float") == 'right') {
                // console.log(this);
            } else {
                if ( $(this).attr('id').match(key, 'g') ) {
                    $(this).addClass("active");
                } else {
                    $(this).removeClass("active");
                }
            }
        });
        var items = [];
        if ( key == 'about' ) {
            $.each(data, function(key, val) {
                if ( val.id.match(/^joko_.*$/g) ) {
                    items.push('<a href="/board/' + val.id + '/post/list"><span>' + val.name + '</span></a>');
                } else {
                    items.push('<a href="/joko/' + val.id + '"><span>' + val.name + '</span></a>');
                }
            });
        } else {
            $.each(data, function(key, val) {
                items.push('<a href="/board/' + val.id + '/post/list"><span>' + val.name + '</span></a>');
            });
        }
        $(items.join('\n')).appendTo("#middlebar");
    });
}
