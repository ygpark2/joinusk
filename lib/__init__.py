from django.conf import settings
from joko.board.documents import Post

def csrf_post_filter(post):
    tmp_post = post.copy()
    del tmp_post['csrfmiddlewaretoken']
    return tmp_post

def reply_count(post_id, cnt):
    settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_post'
    post = Post.documents.get(post_id)
    post.replycnt = post.replycnt + 1
    return post.save()
