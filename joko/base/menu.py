# -*- coding:utf-8 -*-

BOARD_LIST = [
    {'id' : 'jisik_travel_place', 'name' : '관광여행 여행지', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_travel_house', 'name' : '관광여행 숙박', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_travel_food', 'name' : '관광여행 먹거리', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_travel_activity', 'name' : '관광여행 기타활동', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_shopping', 'name' : '쇼핑', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_jobs', 'name' : '일자리', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_realestate', 'name' : '부동산', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_transportation', 'name' : '교통', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_medic', 'name' : '의료/보험', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_culture', 'name' : '문화/공연', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_province', 'name' : '지역', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_education', 'name' : '교육', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_it', 'name' : 'IT/통신', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_counsel', 'name' : '고민상담', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_labourlawyer', 'name' : '전문가 노무사', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_solicitor', 'name' : '전문가 법무사', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jisik_lawyer', 'name' : '전문가 변호사', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},

    {'id' : 'jkt_notice_local', 'name' : '알림마당 지역소식방', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jkt_notice_policy', 'name' : '알림마당 정책방', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jkt_ucc_free', 'name' : 'UCC 자유게시판', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jkt_ucc_column', 'name' : 'UCC 컬럼', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jkt_market_buy', 'name' : '중고장터 삽니다', 'description' : '', 'enable_upload': True, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jkt_market_sell', 'name' : '중고장터 팝니다', 'description' : '', 'enable_upload': True, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jkt_job_jobseeker', 'name' : '구인/구직란 일자리구함', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},
    {'id' : 'jkt_job_employer', 'name' : '구인/구직란 사람구함', 'description' : '', 'enable_upload': False, 'enable_comment': True, 'enable_opinion': True},

    {'id' : 'joko_notice', 'name' : '공지사항', 'description' : '', 'enable_upload': False, 'enable_comment': False, 'enable_opinion': False},
    {'id' : 'joko_faq', 'name' : '자주묻는 질문', 'description' : '', 'enable_upload': False, 'enable_comment': False, 'enable_opinion': False},
    {'id' : 'joko_qna', 'name' : '문의게시판', 'description' : '', 'enable_upload': False, 'enable_comment': False, 'enable_opinion': False},
    # {'id' : '', 'name' : '', 'description' : ''},
]

JOKO_ABOUT = [
    {'id' : 'introduction', 'name' : '소개', 'url' : '', 'description' : ''},
    {'id' : 'joko_notice', 'name' : '공지사항', 'url' : '', 'description' : ''},
    {'id' : 'joko_faq', 'name' : 'FAQ', 'url' : '', 'description' : ''},
    {'id' : 'joko_qna', 'name' : '문의게시판', 'url' : '', 'description' : ''},
    {'id' : 'coalition', 'name' : '제휴 및 광고문의', 'url' : '', 'description' : ''},
    {'id' : 'provision', 'name' : '약관/운영원칙', 'url' : '', 'description' : ''},
    {'id' : 'sitemap', 'name' : '사이트맵', 'url' : '', 'description' : ''},
    {'id' : 'secession', 'name' : '회원탈퇴', 'url' : '', 'description' : ''},
]
