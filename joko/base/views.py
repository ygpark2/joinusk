from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils import simplejson
from joko.base.menu import BOARD_LIST, JOKO_ABOUT

from django.contrib.auth.forms import AuthenticationForm
from registration.forms import RegistrationForm

from django.contrib.auth.views import login, logout
from django.views.decorators.csrf import csrf_protect
from session_csrf import anonymous_csrf

@anonymous_csrf
def home(request, extra_context=None):
    # 'backend': 'registration.backends.default.DefaultBackend'

    register_form = RegistrationForm()
    login_form=AuthenticationForm(request)
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    return render_to_response( 'index.html',
                              { 'login_form': login_form, 'register_form': register_form },
                              context_instance=context)

def top_menu(request, menu_id):
    if request.is_ajax():
        if ( cmp('about', menu_id) == 0 ):
            return HttpResponse(simplejson.dumps(JOKO_ABOUT), mimetype='text/json')
        else:
            filtered_menu = filter(lambda x: x['id'].startswith(menu_id), BOARD_LIST)
            return HttpResponse(simplejson.dumps(filtered_menu), mimetype='text/json')
    else:
        return HttpResponseRedirect(reverse('home'))

def activate(request, activation_key, template_name='registration/activate.html', extra_context=None):

    activation_key = activation_key.lower() # Normalize before trying anything with it.
    account = RegistrationProfile.activate_user(activation_key)
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value
    return render_to_response(template_name,
                              { 'account': account,
                                'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS },
                              context_instance=context)

