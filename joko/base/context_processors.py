from django.conf import settings # import the settings file
from menu import BOARD_LIST

def joko_menu(context):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'TOP_MENU': BOARD_LIST, 'languages': settings.LANGUAGES}
    # return {'TOP_MENU': settings.BOARD_LIST}
