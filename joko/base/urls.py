from django.conf.urls.defaults import patterns, url, include
from django.conf import settings
from django.views.generic import TemplateView
# from django.views.generic.simple import direct_to_template

# from joko.base.views import top_menu

urlpatterns = patterns('joko.base.views',

    url(r'^$',         		'home',             name='joko.base.home'),
    url(r'^joko/top/menu/(?P<menu_id>\w+)$', 'top_menu', name='joko.base.top_menu'),
    (r'^i18n/',           include('django.conf.urls.i18n')),
    (r'introduction$',   TemplateView.as_view(template_name='about/introduction.html')),
    (r'coalition$',      TemplateView.as_view(template_name='about/coalition.html')),
    (r'provision$',      TemplateView.as_view(template_name='about/provision.html')),
    (r'sitemap$',        TemplateView.as_view(template_name='about/sitemap.html')),
    (r'secession$',      TemplateView.as_view(template_name='about/secession.html')),
    # url(r'^$', 			home, 				name='joko.footer'),

)
