from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect, render_to_response
from django.template.context import RequestContext
# from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required

from datetime import datetime
from django.conf import settings
from django.forms.formsets import formset_factory

from joko.board.documents import Board, Post, Comment
from joko.board.forms import BoardForm, PostForm, CommentForm, CommentOpinionForm

from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.core.urlresolvers import reverse
from django.core import serializers

from lib import csrf_post_filter, reply_count
from django.utils.encoding import smart_str

# @csrf_protect
@login_required(login_url='/accounts/login/')
def board_create(request):
    if request.method == 'POST':
        formset = BoardForm(request.POST)
        if formset.is_valid():
            postVal = csrf_post_filter(request.POST)
            Board(**postVal).save()
            # do something.
    else:
        formset = BoardForm()
    return render_to_response('board/create.html', { 'board_form': formset }, RequestContext(request))

@login_required(login_url='/accounts/login/')
def board_list(request):
    settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_board'
    board_list = Board.documents.all().sort('-regdate')[:10]
    paginator = Paginator(board_list, 10) # Show 10 posts per page

    # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        posts = paginator.page(page)
    except (EmptyPage, InvalidPage):
        posts = paginator.page(paginator.num_pages)

    return render_to_response('board/list.html', {"posts": posts}, RequestContext(request))

@login_required(login_url='/accounts/login/')
def board_view(request, board_id):
    settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_board'
    board = Board.documents.get(board_id)
    comments = Comment.documents.q(board_id__exact=post_id).sort('-regdate')
    formset = CommentForm(initial={'board_id': post_id})
    return render_to_response('board/view.html', {'board': board, 'comments': comments, 'comment_form': formset}, RequestContext(request))
    # print post_id
    # post = Board.documents.q(id__exact=post_id)
    # post = Board.documents.get("21fa8d62-e2ee-4bc1-87ed-4e29b13199b2")

    # print post.title

# @csrf_protect
@login_required(login_url='/accounts/login/')
def post_create(request, board_id):
    settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_board'
    board = Board.documents.get(board_id)
    if request.method == 'POST':
        formset = PostForm(is_upload=board.enable_upload, data=request.POST)
        if formset.is_valid():
            postVal = csrf_post_filter(request.POST)
            postVal['board_id'] = board_id
            postVal['user_id'] = request.user.username
            postVal.setlist('tags', postVal['tags'].split(','))
            postVal.setlist('langs', postVal['langs'].split(','))
            Post(**postVal).save()
            return HttpResponseRedirect(reverse('joko.board.post_list', args=( board_id, ) ))
    else:
        formset = PostForm(is_upload=board.enable_upload)
    return render_to_response('post/create.html', {'post_form': formset, 'board_id': board_id}, RequestContext(request))

@login_required(login_url='/accounts/login/')
def post_list(request, board_id):
    settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_post'
    # post_list = Board.documents.all().sort('regdate')
    post_list = Post.documents.q(board_id__exact=board_id).sort('-regdate')[:10]
    paginator = Paginator(post_list, 10) # Show 10 posts per page

    # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        posts = paginator.page(page)
    except (EmptyPage, InvalidPage):
        posts = paginator.page(paginator.num_pages)

    return render_to_response('post/list.html', {"posts": posts, "board_id": board_id}, RequestContext(request))

@login_required(login_url='/accounts/login/')
def post_view(request, board_id, post_id):
    if request.method == 'POST':
        formset = CommentForm(request.POST)
        if formset.is_valid():
            postVal = request.POST.copy()
            Board(**postVal).save()
            # do something.
    else:
        settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_post'
        # increase post view count
        post = Post.documents.get(post_id)
        post.viewcnt = post.viewcnt + 1
        post.save()

        settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_post_comment'
        comments = Comment.documents.q(post_id__exact=post_id).sort('-regdate')

        comment_formset = CommentForm(initial={'board_id': board_id, 'post_id': post_id})
        opinion_formset = CommentOpinionForm(initial={'user_id': 'test_user_id_fix_me!'})
        return render_to_response('post/view.html', {'board_id': board_id, 'post': post, 'comments': comments, 'comment_form': comment_formset, 'opinion_form': opinion_formset}, RequestContext(request))

@login_required(login_url='/accounts/login/')
def post_edit(request, board_id, post_id):
	settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_board'
	board = Board.documents.get(board_id)

	settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_post'
	post = Post.documents.get(post_id)
	if (request.user.username == post.user_id):
		if request.method == 'POST':
				formset = PostForm(is_upload=board.enable_upload, data=request.POST)
				postVal = csrf_post_filter(request.POST)
				if formset.is_valid():
					post.title = postVal['title']
					post.email = postVal['email']
					post.writer = postVal['writer']
					post.tags = postVal['tags'].split(',')
					post.langs = postVal['langs'].split(',')
					post.content = postVal['content']
					post.save()
					return HttpResponseRedirect(reverse('joko.board.post_view', args=(board_id, post_id, )))
				else:
					formset = PostForm(initial=postVal)
		else:
			data = {
				'title': post.title,
				'email': post.email,
				'writer': post.writer,
				'tags': ", ".join(post.tags),
				'langs': ", ".join(post.langs),
				'content': post.content
			}
	        formset = PostForm(initial=data)
		return render_to_response('post/edit.html', {'board_id': board_id, 'post_id': post_id, 'post_form': formset}, RequestContext(request))
	else:
		return HttpResponseRedirect(reverse('joko.board.post_view', args=(board_id, post_id, )))

@login_required(login_url='/accounts/login/')
def post_delete(request, board_id, post_id):
	settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_post'
	post = Post.documents.get(post_id)
	if (request.user.username == post.user_id):
		if request.method == 'POST':
			settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_post_comment'
			comments = Comment.documents.q(post_id__exact=post_id)
			for comment in comments:
				comment.delete()

			post.delete()
			return HttpResponseRedirect(reverse('joko.board.post_list', args=(board_id, )))
		else:
			return render_to_response('post/delete.html', {'board_id': board_id, 'post_id': post_id}, RequestContext(request))
	else:
		return HttpResponseRedirect(reverse('joko.board.post_view', args=(board_id, post_id, )))

@login_required(login_url='/accounts/login/')
def post_recommend(request, board_id, post_id):
	settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + 'joko_post'
	post = Post.documents.get(post_id)
	if request.method == 'POST':
		post.votecnt = post.votecnt + int(request.POST['votecnt'])
		post.save()
		return HttpResponseRedirect(reverse('joko.board.post_view', args=(board_id, post_id, )))
	else:
		return HttpResponseRedirect(reverse('joko.board.post_view', args=(board_id, post_id, )))


@login_required(login_url='/accounts/login/')
def comment_create(request):
    if request.method == 'POST':
        formset = CommentForm(request.POST)
        if formset.is_valid():
            postVal = csrf_post_filter(request.POST)
            del postVal['board_id']
            postVal['user_id'] = request.user.username
            Comment(**postVal).save()
            reply_count(postVal.get('post_id'), 1)
            return HttpResponseRedirect(reverse('joko.board.post_view', args=(request.POST['board_id'], request.POST['post_id'], )))
    else:
        redirect('board.views', post_id=request.POST['post_id'])

@login_required(login_url='/accounts/login/')
def opinion_create(request):
	if request.method == 'POST':
		formset = CommentOpinionForm(request.POST)
		if formset.is_valid():
			postVal = csrf_post_filter(request.POST)

			comment = Comment.documents.get(postVal['comment_id'])
			del postVal['comment_id']

			from django.core.serializers.json import DjangoJSONEncoder
			from django.utils import simplejson as json

			postVal['update_time'] = datetime.now()
			if comment.opinions is None:
				comment.opinions = json.dumps(postVal, separators=(',',':'), cls=DjangoJSONEncoder)
			else:
				comment.opinions.append( json.dumps(postVal, separators=(',',':'), cls=DjangoJSONEncoder) )

			# comment.opinions = None
			comment.save()

			return HttpResponseRedirect(reverse('joko.board.post_view', args=(request.POST['board_id'], request.POST['post_id'],)))
	else:
		redirect('board.views', post_id=request.POST['post_id'])
