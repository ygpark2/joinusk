"""
A management command which deletes expired accounts (e.g.,
accounts which signed up but never activated) from the database.

Calls ``RegistrationProfile.objects.delete_expired_users()``, which
contains the actual logic for determining which accounts are deleted.

"""

from django.core.management.base import BaseCommand

from optparse import make_option
import cStringIO as StringIO
import socket, time, urllib, urlparse

from joko.board.documents import Board
from joko.base.menu import BOARD_LIST
# from registration.documents import RegistrationProfile
from datetime import datetime

class Command(BaseCommand):
    help = "Delete expired user registrations from the database"

    base_options = (
        make_option("-f", "--filename", action="store", type="string", dest="filename",
                    help='If provided, directs output to a file instead of stdout.'),
        make_option("-u", "--using", action="store", type="string", dest="using", default=DEFAULT_ALIAS,
                    help='If provided, chooses a connection to work with.'),
    )
    option_list = BaseCommand.option_list + base_options

    readable = True
    writeable = True
    remote_schema_file = "admin/file/?file=schema.xml"

    def handle(self, **options):
        """Generates a Solr schema that reflects the indexes."""
        using = options.get('using')
        schema_xml = self.build_template(using=using)

        if options.get('filename'):
            self.write_file(options.get('filename'), schema_xml)
        else:
            self.print_stdout(schema_xml)

    def __init__(self, url, schemadoc=None, http_connection=None, mode='', retry_timeout=-1, max_length_get_url=MAX_LENGTH_GET_URL):
        self.conn = SolrConnection(url, http_connection, retry_timeout, max_length_get_url)
        self.schemadoc = schemadoc
        if mode == 'r':
            self.writeable = False
        elif mode == 'w':
            self.readable = False
        self.init_schema()

    def init_schema(self):
        if self.schemadoc:
            schemadoc = self.schemadoc
        else:
            r, c = self.conn.request( urlparse.urljoin(self.conn.url, self.remote_schema_file) )
            if r.status != 200:
                raise EnvironmentError("Couldn't retrieve schema document from server - received status code %s\n%s" % (r.status, c))
            schemadoc = StringIO.StringIO(c)
        self.schema = SolrSchema(schemadoc)