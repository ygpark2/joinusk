"""
A management command which deletes expired accounts (e.g.,
accounts which signed up but never activated) from the database.

Calls ``RegistrationProfile.objects.delete_expired_users()``, which
contains the actual logic for determining which accounts are deleted.

"""

from django.core.management.base import NoArgsCommand

from joko.board.documents import Board
from joko.base.menu import BOARD_LIST
# from registration.documents import RegistrationProfile
from datetime import datetime

class Command(NoArgsCommand):
    help = "Delete expired user registrations from the database"

    def handle_noargs(self, **options):
        for board in BOARD_LIST:
            board['regdate'] = datetime.now()
            board['moddate'] = datetime.now()
            board['posts'] = 0
            board['comments'] = 0
            Board(**board).save()
