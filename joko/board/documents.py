# coding: utf-8

from djangosolr.documents import *
from django.conf import settings
from django.core.urlresolvers import reverse

from uuid import uuid4
from datetime import datetime

from django.utils import simplejson as json

class Board(Document):

    """ 게시판 기본 저장 구조

    :param foo: A string to be converted
    :returns: A bar formatted string
    """

    id = CharField(primary_key=True)
    # admin_id = CharField()
    name = CharField()
    description = TextField()
    enable_upload = BooleanField()
    enable_comment = BooleanField()
    enable_opinion = BooleanField()
    regdate = DateTimeField()
    moddate = DateTimeField()
    posts = IntegerField()
    post_regdate = DateTimeField()
    comments = IntegerField()
    comment_regdate = DateTimeField()
    text = TextField(stored=False)

    def pre_save(self):

        """ 게시판 생성 시 실행되는 함수

        :param foo: A string to be converted
        :returns: A bar formatted string
        """
        settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + "joko_board"
        if self.id == None:            # insert new data
            self.id=uuid4()
            self.regdate=datetime.now()
            self.moddate=datetime.now()
        else:                          # update data
            self.moddate=datetime.now()

    def get_absolute_url(self):
        return reverse('board.post_view', args=[self.id])

    class Meta:
        type = 'board'

class Post(Document):

    id = CharField(primary_key=True)
    board_id = CharField()
    user_id = CharField()
    title = CharField()
    email= CharField()
    writer = CharField()
    tags = CharField(multivalued=True)
    files = CharField(multivalued=True)
    content = TextField()
    langs = CharField(multivalued=True)
    regdate = DateTimeField()
    moddate = DateTimeField()
    viewcnt = IntegerField()
    votecnt = IntegerField()
    spoint = IntegerField()
    replycnt = IntegerField()
    text = TextField(stored=False)

    def pre_save(self):
        settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + "joko_post"
        if self.id == None:            # insert new data
            self.id=uuid4()
            self.viewcnt=0
            self.votecnt=0
            self.replycnt=0
            self.spoint=0
            self.regdate=datetime.now()
            self.moddate=datetime.now()
        else:                          # update data
            self.moddate=datetime.now()

    def get_absolute_url(self):
        return reverse('joko.board.post_view', args=[self.board_id, self.id])

    class Meta:
        type = 'post'

class Comment(Document):

    id = CharField(primary_key=True)
    post_id = CharField()
    user_id = CharField()
    writer = CharField()
    email = CharField()
    content = TextField()
    opinions = CharField(multivalued=True)
    select = BooleanField()
    regdate = DateTimeField()
    moddate = DateTimeField()
    text = TextField(stored=False)

    def pre_save(self):
        settings.DJANGOSOLR_URL = settings.DJANGOSOLR_ROOT_URL + "joko_post_comment"
        if self.id == None:
            self.id = uuid4()
            self.select = False
            self.regdate=datetime.now()
            self.moddate=datetime.now()
        else:
            self.moddate=datetime.now()

    class Meta:
        type = 'postcomment'
