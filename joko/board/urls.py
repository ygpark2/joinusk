from django.conf.urls.defaults import patterns, include, url

# from board.views import board_create, board_list, board_view, post_create, post_list, post_view, comment_create, opinion_create

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('joko.board.views',
    # Examples:
    url(r'^create$', 'board_create', name='joko.board.create'),
    url(r'^list$', 'board_list', name='joko.board.list'),
    url(r'^view/(?P<board_id>\w+)$', 'board_view', name='joko.board.view'),

    url(r'^(?P<board_id>\w+)/post/list$', 'post_list', name='joko.board.post_list'),
    url(r'^(?P<board_id>\w+)/post/create$', 'post_create', name='joko.board.post_create'),
    url(r'^(?P<board_id>\w+)/post/delete/(?P<post_id>[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12})$', 'post_delete', name='joko.board.post_delete'),
    url(r'^(?P<board_id>\w+)/post/edit/(?P<post_id>[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12})$', 'post_edit', name='joko.board.post_edit'),
    url(r'^(?P<board_id>\w+)/post/view/(?P<post_id>[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12})$', 'post_view', name='joko.board.post_view'),
	url(r'^(?P<board_id>\w+)/post/recommend/(?P<post_id>[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12})$', 'post_recommend', name='joko.board.post_recommend'),

    url(r'^comment/create$', 'comment_create', name='joko.board.comment_create'),
    url(r'^opinion/create$', 'opinion_create', name='joko.board.opinion_create'),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
