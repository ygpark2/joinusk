#!/bin/sh

SCHEMA_URL=http://222.108.177.54:8983/solandra/schema/

KEY_PREFIX=joko_

for filePath in ./schema/*.xml ; do
    fileName=${filePath##*/}
    schemaKey=${fileName%%.*}
    curl $SCHEMA_URL$KEY_PREFIX$schemaKey --data-binary @$filePath -H 'Content-type:text/xml; charset=utf-8'
done
