# -*- coding: utf-8 -*-

# from django.forms import ModelForm, Textarea
from django import forms

class BoardForm(forms.Form):
    board_id = forms.CharField(widget=forms.HiddenInput)
    name = forms.CharField(max_length=250, required=True)
    description = forms.CharField(required=True, widget=forms.Textarea)

class PostForm(forms.Form):
    title = forms.CharField(initial='', max_length=250, required=True)
    email = forms.EmailField(initial='', required=True)
    writer = forms.CharField(initial='', max_length=100, required=True)
    tags = forms.CharField(initial='', max_length=250)
    langs = forms.CharField(initial='', max_length=250)
    content = forms.CharField(initial='', required=True, widget=forms.Textarea)

    def __init__(self, is_edit=False, is_upload=False, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        if is_upload:
            self.fields['files'] = forms.FileField()
        if is_edit:
            self.fields['post_id'] = forms.CharField(widget=forms.HiddenInput)

class CommentForm(forms.Form):
    board_id = forms.CharField(widget=forms.HiddenInput)
    post_id = forms.CharField(widget=forms.HiddenInput)
    email = forms.EmailField(required=True)
    writer = forms.CharField(max_length=100, required=True)
    content = forms.CharField(required=True, widget=forms.Textarea)

OPINION_CHOICES = (
    ('aq', '추가질문'),
    ('ao', '추가의견'),
    ('oo', '반대의견'),
)

class CommentOpinionForm(forms.Form):
    user_id = forms.CharField(widget=forms.HiddenInput)
    opinionType = forms.CharField(max_length=10, widget=forms.Select(choices=OPINION_CHOICES), required=True)
    content = forms.CharField(required=True, widget=forms.Textarea)

SEARCH_CHOICES = (
    ('title', '제목'),
    ('writer', '작성자'),
    ('content', '본문'),
    ('text', '전체'),
)

class SearchForm(forms.Form):
    searchField = forms.CharField(max_length=10, widget=forms.Select(choices=SEARCH_CHOICES), required=True)
    searchValue = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
