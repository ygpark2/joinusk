# -*- coding: utf-8 -*-
#
# Copyright (c) 2009 Arthur Furlan <arthur.furlan@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2
#
#
# Based on the django's default templatetags ifequal and ifnotequal.

# from django.template.base import (Node, NodeList, Template, Context, Library,
#     TemplateSyntaxError, VariableDoesNotExist, InvalidTemplateLibrary,
#     BLOCK_TAG_START, BLOCK_TAG_END, VARIABLE_TAG_START, VARIABLE_TAG_END,
#     SINGLE_BRACE_START, SINGLE_BRACE_END, COMMENT_TAG_START, COMMENT_TAG_END,
#     VARIABLE_ATTRIBUTE_SEPARATOR, get_library, token_kwargs, kwarg_re,
#     render_value_in_context)

from django.template.base import Variable, Library, VariableDoesNotExist
from joko.board.forms import OPINION_CHOICES
from django.utils import simplejson as json

import re

register = Library()

@register.filter
def opinionTypeName(value):
	for tynm in OPINION_CHOICES:
		if tynm[0] == value:
			return tynm[1]

@register.filter
def opiniondict(value):
	print "option list"
	return json.loads(value)
