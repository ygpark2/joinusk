
# Joko Project #

## About ##

Describe your project here.

## Prerequisites ##

- Python >= 2.5
- pip
- virtualenv (virtualenvwrapper is recommended for use during development)

## Installation ##

1. sudo pip virtualenv
2. virtualenv --no-site-packages venv
3. ./venv/bin/pip install -r requirements/dev.txt

### Window ###

- disable ( celery, django-celery )
- py-bcrypt 
- 


License
-------
This software is licensed under the [New BSD License][BSD]. For more
information, read the file ``LICENSE``.

[BSD]: http://opensource.org/licenses/BSD-3-Clause
