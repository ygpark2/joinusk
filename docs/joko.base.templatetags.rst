templatetags Package
====================

:mod:`joko_tags` Module
-----------------------

.. automodule:: joko.base.templatetags.joko_tags
    :members:
    :undoc-members:
    :show-inheritance:

