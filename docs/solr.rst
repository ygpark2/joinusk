========================================
Solr 문서
========================================

기본적으로 조인어스 코리아 시스템은 데이터 베이스가 아닌 solr검색엔진을 데이터 저장 용도로 사용하고 있습니다.

Solr는 검색엔진을 목적으로 만들어진 소프트웨어라 검색에 있어서는 빠른 응답속도를 보여줍니다. 그래서 검색이 빈번하게 일어나는 사이트에서는 최적의 선택이라 할 수 있습니다.
다만 기존의 데이터 베이스와 달리 데이터 구조화 작업을 Solr기반에서 하는 것이 어느정도 기술적인 어려움으로 여겨지고 있습니다. 그리고 이 구조화 작업은 나중에 검색에 최적화 될 수 있도록 고려되어야 하기 때문에 항상 주의깊게 여러 경우를 생각한 후 설계되어야 합니다.


설치
-------------

`SolrCloud <http://wiki.apache.org/solr/SolrCloud>`_.

SolrCloud는 Solr의 분산 처리 기능의 이름입니다. SolrCloud에서 Solr서버들의 fault tolerant 클러스트를 구성할 수 있습니다. 아주 높은 스케일 기능과 분산 인텍싱 그리고 검색 기능을 사용하기 위해서는 solr 클라우드를 선택하셔야 합니다.

조인어스 코리아 시스템은 SolrCloud기능을 이용하여 Solr를 설치하였지만 하드웨어의 제약으로 인해 클러스트를 구성하지는 못하였습니다. 추후 데이터가 많아지면 클러스트 구성하여 확장하면 됩니다.

자세한 설치 절차는 위 문서의 Example A를 따라하시면 됩니다.

스키마
--------

joko_board
~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8" ?>
   <schema name="joko_board" version="1.5">
   <fields>
    <field name="id" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="type" type="string" indexed="true" stored="true" multiValued="false" required="true"/>

    <field name="board__id" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="board__admin_id" type="string" indexed="true" stored="true" multiValued="false" />
    <field name="board__name" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="board__description" type="text_general" indexed="true" stored="true" multiValued="false" />
    <field name="board__enable_upload" type="boolean" indexed="true" stored="true" required="true"/>
    <field name="board__enable_comment" type="boolean" indexed="true" stored="true" required="true"/>
    <field name="board__enable_opinion" type="boolean" indexed="true" stored="true" required="true"/>
    <field name="board__regdate" type="date" indexed="true" stored="true" required="true"/>
    <field name="board__moddate" type="date" indexed="true" stored="true" required="true"/>
    <field name="board__posts" type="tdouble" indexed="true" stored="true" required="true"/>
    <field name="board__post_regdate" type="date" indexed="true" stored="true" />
    <field name="board__comments" type="tdouble" indexed="true" stored="true" required="true"/>
    <field name="board__comment_regdate" type="date" indexed="true" stored="true" />

    <!-- catchall field, containing all other searchable text fields (implemented
        via copyField further on in this schema  -->
   <field name="board__text" type="text_general" indexed="true" stored="false" multiValued="true"/>
   <field name="timestamp" type="date" indexed="true" stored="true" default="NOW" multiValued="false"/>

   <field name="board__version" type="long" indexed="true" stored="true"/>

   <field name="_version_" type="long" indexed="true" stored="true"/>
   ...
   </fields>
   </schema>

joko_post
~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8" ?>
   <schema name="joko_post" version="1.5">
   <fields>
    <field name="id" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="type" type="string" indexed="true" stored="true" multiValued="false" required="true"/>

    <field name="post__id" type="string" indexed="true" stored="true" multiValued="false" required="true" />
    <field name="post__user_id" type="string" indexed="true" stored="true" multiValued="false" />
    <field name="post__board_id" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="post__title" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="post__email" type="string" indexed="true" stored="true" multiValued="false" />
    <field name="post__writer" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="post__tags" type="string" indexed="true" stored="true" multiValued="true" />
    <field name="post__files" type="string" indexed="true" stored="true" multiValued="true" />
    <field name="post__content" type="text_general" indexed="true" stored="true" multiValued="false" required="true" />
    <field name="post__langs" type="string" stored="true" indexed="true" multiValued="true" />
    <field name="post__regdate" type="date" indexed="true" stored="true" required="true" />
    <field name="post__moddate" type="date" indexed="true" stored="true" required="true" />
    <field name="post__viewcnt" type="tint" indexed="true" stored="true" required="true" />
    <field name="post__votecnt" type="tint" indexed="true" stored="true" required="true" />
    <field name="post__replycnt" type="tint" indexed="true" stored="true" required="true" />
    <field name="post__spoint" type="tint" indexed="true" stored="true" required="true" />

   <field name="post__text" type="text_general" indexed="true" stored="false" multiValued="true"/>
   <field name="post__version" type="long" indexed="true" stored="true"/>
   <field name="_version_" type="long" indexed="true" stored="true"/>
   ...
   </fields>
   </schema>

joko_post_commnet
~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: xml

   <?xml version="1.0" encoding="UTF-8" ?>
   <schema name="joko_post_comment" version="1.5">
   <fields>
    <field name="id" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="type" type="string" indexed="true" stored="true" multiValued="false" required="true"/>

    <field name="post__id" type="string" indexed="true" stored="true" multiValued="false" required="true" />
    <field name="post__user_id" type="string" indexed="true" stored="true" multiValued="false" />
    <field name="post__board_id" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="post__title" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="post__email" type="string" indexed="true" stored="true" multiValued="false" />
    <field name="post__writer" type="string" indexed="true" stored="true" multiValued="false" required="true"/>
    <field name="post__tags" type="string" indexed="true" stored="true" multiValued="true" />
    <field name="post__files" type="string" indexed="true" stored="true" multiValued="true" />
    <field name="post__content" type="text_general" indexed="true" stored="true" multiValued="false" required="true" />
    <field name="post__langs" type="string" stored="true" indexed="true" multiValued="true" />
    <field name="post__regdate" type="date" indexed="true" stored="true" required="true" />
    <field name="post__moddate" type="date" indexed="true" stored="true" required="true" />
    <field name="post__viewcnt" type="tint" indexed="true" stored="true" required="true" />
    <field name="post__votecnt" type="tint" indexed="true" stored="true" required="true" />
    <field name="post__replycnt" type="tint" indexed="true" stored="true" required="true" />
    <field name="post__spoint" type="tint" indexed="true" stored="true" required="true" />

   <field name="post__text" type="text_general" indexed="true" stored="false" multiValued="true"/>
   <field name="post__version" type="long" indexed="true" stored="true"/>
   <field name="_version_" type="long" indexed="true" stored="true"/>
   ...
   </fields>
   </schema>
