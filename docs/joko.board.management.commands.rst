commands Package
================

:mod:`boarddata` Module
-----------------------

.. automodule:: joko.board.management.commands.boarddata
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`createschema` Module
--------------------------

.. automodule:: joko.board.management.commands.createschema
    :members:
    :undoc-members:
    :show-inheritance:

