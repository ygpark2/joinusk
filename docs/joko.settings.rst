settings Package
================

:mod:`settings` Package
-----------------------

.. automodule:: joko.settings
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`base` Module
------------------

.. automodule:: joko.settings.base
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`local-dist` Module
------------------------

.. automodule:: joko.settings.local-dist
    :members:
    :undoc-members:
    :show-inheritance:

