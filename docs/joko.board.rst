board Package
=============

:mod:`documents` Module
-----------------------

.. automodule:: joko.board.documents
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`forms` Module
-------------------

.. automodule:: joko.board.forms
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`models` Module
--------------------

.. automodule:: joko.board.models
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`tests` Module
-------------------

.. automodule:: joko.board.tests
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`urls` Module
------------------

.. automodule:: joko.board.urls
    :members:
    :undoc-members:
    :show-inheritance:

:mod:`views` Module
-------------------

.. automodule:: joko.board.views
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    joko.board.management
    joko.board.templatetags

