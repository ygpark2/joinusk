========================================
Welcome to this project's documentation!
========================================

This is a documentation template for a **web application using Django 1.4**.
Feel free to change this to your liking.

이 문서는 조인어스 코리아 문서입니다.


About
-------------

This project uses `Django <http://www.djangoproject.com/>`_.

Contents
--------

.. toctree::
   :maxdepth: 2

   solr.rst
   joko.rst

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
