#!/bin/zsh

dir_keys=("joko_board" "joko_post" "joko_post_comment" "uniqna")

echo "----------------------------------"
echo $1
echo "----------------------------------"

for key in $dir_keys; do
    if [ $1 = "c" ]; then
        echo "====== create default dir ======="
        mkdir -m 775 ./$key/data
        chown www-data:users ./$key/data
    else
        echo "====== delete default dir ======="
        rm -rf ./$key/data
    fi
done

if [ $1 = "c" ]; then
    chown www-data:users ./zoo_data
    chmod 775 ./zoo_data
else
    rm -rf ./zoo_data/*
fi
