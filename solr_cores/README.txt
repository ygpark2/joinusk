This is an alternative setup structure to support multiple cores.

To run this configuration, start jetty in the example/ directory using:

1. To put solr_cores folder into example folder.

2. java -Dsolr.solr.home=solr_cores -jar start.jar

For general examples on standard solr configuration, see the "solr" directory.
